/**
 * Created by Prajan on 3/25/2015.
 */

var app = angular.module('familyApp', ['ngRoute']);

//This configures the routes and associates each route with a view and a controller
app.config(function ($routeProvider) {
    $routeProvider
        .when('/families',
        {
            controller: 'FamiliesController',
            templateUrl: 'family-app/partials/families.html'
        })
        //Define a route that has a route parameter in it (:customerID)
        .when('/families/:familyID',
        {
            controller: 'FamilyDetailsController',
            templateUrl: 'family-app/partials/familyDtls.html'
        })
        .when('/person',
        {
            controller: 'PersonController',
            templateUrl: 'family-app/partials/person.html'
        })
        .otherwise({ redirectTo: '/families' });
});

