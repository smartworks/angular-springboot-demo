﻿//This handles retrieving data and is used by controllers. 3 options (server, factory, provider) with 
//each doing the same thing just structuring the functions/data differently.
app.service('familyService', function ($http) {
    this.getFamilies = function () {        
    	return $http.get('/families/');     	
    };

    this.getFamily = function (id) {
    	return $http.get('/families/'+id);  
    };
});


app.service('personService', function($http) {
	return {
		getPerson : function() {
			return $http.get('/person/');
		}
	}
});