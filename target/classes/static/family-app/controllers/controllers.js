﻿/*#######################################################################
  
  Dan Wahlin
  http://twitter.com/DanWahlin
  http://weblogs.asp.net/dwahlin
  http://pluralsight.com/training/Authors/Details/dan-wahlin

  Normally like the break AngularJS controllers into separate files.
  Kept them together here since they're small and it's easier to look through them.
  example. 

  #######################################################################*/

//This controller retrieves data from the customersService and associates it with the $scope
//The $scope is ultimately bound to the customers view
app.controller('FamiliesController', function ($scope, familyService) {

    //I like to have an init() for controllers that need to perform some initialization. Keeps things in
    //one place...not required though especially in the simple example below
    init();
    function init() {
        $scope.families; 
        var promise = familyService.getFamilies();
        promise.then(function(payload) {
			$scope.families = payload.data;
		}, function(errorPayload) {
			$log.error('failure loading person', errorPayload);
		});
    }

});

//This controller retrieves data from the customersService and associates it with the $scope
//The $scope is bound to the order view
app.controller('FamilyDetailsController', function ($scope, $routeParams, familyService) {
    $scope.family = {};

    //I like to have an init() for controllers that need to perform some initialization. Keeps things in
    //one place...not required though especially in the simple example below
    init();

    function init() {
        //Grab customerID off of the route        
        var familyID = ($routeParams.familyID) ? parseInt($routeParams.familyID) : 0;
        if (familyID > 0) {
            //$scope.family = 
            var returnPromise = familyService.getFamily(familyID);
            returnPromise.then(function(payload) {
    			$scope.family = payload.data; 
    			   			
    		}, function(errorPayload) {
    			$log.error('failure loading person', errorPayload);
    		});
        }
    }

});

app.controller('PersonController',
		function($scope, $routeParams, personService) {
			$scope.person = {};
			init();

			function init() {
				var promise = personService.getPerson();
				promise.then(function(payload) {
					$scope.person = payload.data;
				}, function(errorPayload) {
					$log.error('failure loading person', errorPayload);
				});
			}

			function getPerson() {
				$scope.person = myService.async().data().then();
			}

		});



