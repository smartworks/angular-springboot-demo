package demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import demo.dao.PersonDao;
import demo.entity.Person;

@Service
@Transactional
public class PersonService {

	@Autowired
	private PersonDao personDao;
	
	public Person getPerson() {
		return personDao.getPerson();
	}
	
	public void createFamily(){
		// create family row
		// create family details
	}
	
	public void getFamilies() {
		
	}
	
}
