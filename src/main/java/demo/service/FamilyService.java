package demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import demo.controller.FamilyDetailsWrapper;
import demo.dao.FamilyDao;
import demo.entity.Family;
import demo.entity.FamilyDetails;

@Service
@Transactional
public class FamilyService {

	@Autowired
	private FamilyDao familyDao;

	public List<Family> getFamilies() {
		return familyDao.getAllFamilies();
	}

	public FamilyDetailsWrapper familyDtls(Integer familyId) {

		// Get family name
		// attach the object

		Family family = familyDao.getFamilyObj(familyId);
		List<FamilyDetails> familyDtls = familyDao.getFamilyDtls(familyId);

		FamilyDetailsWrapper wrapper = new FamilyDetailsWrapper();
		wrapper.setFamilyName(family.getFamilyName());
		wrapper.setAddress(family.getAddress());
		wrapper.setMembers(familyDtls);

		return wrapper;

	}

}
