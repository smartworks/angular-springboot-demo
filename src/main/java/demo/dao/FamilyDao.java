package demo.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import demo.entity.Family;
import demo.entity.FamilyDetails;

@Repository
public class FamilyDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public List<Family> getAllFamilies() {
		BeanPropertyRowMapper<Family> familyBeanRowMapper = new BeanPropertyRowMapper<Family>(Family.class); 
		String sql = "SELECT * FROM FAMILY";
		return jdbcTemplate.query(sql, familyBeanRowMapper);

	}

	public List<FamilyDetails> getFamilyDtls(Integer familyId) { 
		String sql = "select * from FAMILY_DTLS where FAMILYID = ?"; 
		BeanPropertyRowMapper<FamilyDetails> familyBeanRowMapper = new BeanPropertyRowMapper<FamilyDetails>(FamilyDetails.class); 
		return jdbcTemplate.query(sql, new Object[] { familyId }, familyBeanRowMapper);

	}

	public Family getFamilyObj(Integer familyId) {
		BeanPropertyRowMapper<Family> familyBeanRowMapper = new BeanPropertyRowMapper<Family>(Family.class); 
		return jdbcTemplate.queryForObject("select * from Family where id = ?", new Object[]{familyId}, familyBeanRowMapper);
	}

}
