package demo.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import demo.entity.Person;

@Repository
public class PersonDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public Person getPerson() { 		
		String sql = "SELECT * FROM Persons WHERE ID = ?"; 		
		Person person = jdbcTemplate.queryForObject(sql, new Object[] { 2}, new BeanPropertyRowMapper(Person.class));
		return person;		
		
	}
	
	
}
