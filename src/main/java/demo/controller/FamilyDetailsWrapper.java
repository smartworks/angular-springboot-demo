package demo.controller;

import java.util.List;

import demo.entity.FamilyDetails;

public class FamilyDetailsWrapper {
	
	private String familyName;
	private String address;
	private List<FamilyDetails> members;
	
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public List<FamilyDetails> getMembers() {
		return members;
	}
	public void setMembers(List<FamilyDetails> members) {
		this.members = members;
	}
	
	
	
	

}
