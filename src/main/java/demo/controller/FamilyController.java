package demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import demo.entity.Family;
import demo.entity.FamilyDetails;
import demo.service.FamilyService;

@RestController
public class FamilyController {

	@Autowired
	private FamilyService familyService;
	
	@RequestMapping("/families")
	public List<Family> getFamilies() {
		return familyService.getFamilies();
	}
	
	@RequestMapping("/families/{familyId}")
	public FamilyDetailsWrapper getFamilyDtls(@PathVariable("familyId") Integer familyId) {
		return familyService.familyDtls(familyId);
	}
	
}
