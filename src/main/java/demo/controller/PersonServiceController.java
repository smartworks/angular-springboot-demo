package demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import demo.entity.Person;
import demo.service.PersonService;

@RestController
public class PersonServiceController {

	@Autowired
	private PersonService personService;

	@RequestMapping("/person")
	public Person person() {
		return personService.getPerson();
	}
}
